# Ansible role for RatticDB installation

## Introduction

[RatticDB](http://rattic.org/) is a web password manager. It is a WSGI (Python) application with a MySQL/MariaDB database.

This role installs and configure the application and the database, but leaves the web server configuration details to your care.

## LDAP Authentication support

If the `ldap` structure is defined in the parameters, then LDAP authentication is activated. RatticDB always falls back to accounts defined in the SQL database if the account if not found in the LDAP directory.

This role does not handle all possible custom LDAP parameters, but focus on the cases where the directory follows common standards.

The `base_dn` field is used to construct the DN containing users and groups, by prepending respectively the OU 'users' or 'Groups'. The user entry is expected to have its RDN defined using the 'id' attribute, and users firstname and lastname to be defined in the normalized 'givenName' and 'sn' attributes. Groups are expected to be defined using the 'posixGroup' objectclass.

For security reasons, the LDAP connection uses TLS. The legacy LDAPS port is not supported, the STARTTLS extension is used instead.

## Variables

  - **install_path**: wanted path of vhost data installation
      -> you can use the httpd role to prepare the vhost config using this path
      -> using this same role the WSGI path would be <install_path>/ratticweb/wsgi.py
  - **user**: UNIX user to create in order to run the WSGI application
      -> just provide the same username to the httpd role for the WSGI user and you're done
  - **ldap**: this structure if defined activates LDAP authentication; it expects the following fields:
    + server: server name/IP
    + base_dn: LDAP base DN
    + use_ldap_groups: use LDAP groups (default to false)
        beware using LDAP groups unfortunately disables manual management of groups in the SQL database
        (already created groups are still usable but cannot be edited)

